var express = require('express');
var router = express.Router();
var MyM = require('../manager/mysqlManager');
var MoM = require('../manager/mongoManager');

/*
    MySQL tests
 */
router.get('/mysql/insert', function (req, res, next) {
    MyM.testInsertPerformance(function(err, callback) {
        res.render('index.html.twig');
    });
});

router.get('/mysql/backup', function (req, res, next) {
    MyM.backup(function(err, callback) {
        res.render('index.html.twig');
    });
});

/*
    MongoDB tests
 */
router.get('/mongo/insert', function(req, res, next) {
    MoM.testInsertPerformance(function(err, callback) {
        res.render('index.html.twig');
    })
});

router.get('/mongo/backup', function(req, res, next) {
    MoM.backup(function(err, callback) {
        res.render('index.html.twig');
    })
});

module.exports = router;
