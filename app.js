var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();
var port = 9875 || 8000;

// view engine setup
app.set('view engine', 'twig');
app.set('views', path.join(__dirname, 'views'));

//// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'database')));


app.use(require('./routes/index.js'));

app.listen(port, function () {
    console.log('--=[ SERVER STARTED AT ' + port + ' ]=--');
});

module.exports = app;
