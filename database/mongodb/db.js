var mongoose = require('mongoose');
    mongoose.Promise = require('bluebird');
    mongoose.connect('mongodb://localhost:27017/mongotest');
var db = mongoose.connection;

module.exports.getConnection = function(callback) {
    console.log('TRYING TO CONNECT');
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
    });
    return callback(mongoose);
};
