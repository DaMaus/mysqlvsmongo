var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://damaus:thispasswordisswag!@ds111851.mlab.com:11851/conference');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
});

var userSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    woonplaats: String
});

module.exports = mongoose.model('Mongotest', userSchema);