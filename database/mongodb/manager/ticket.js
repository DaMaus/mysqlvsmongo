var TModel = require('../models/ticket');
var Async = require('async');
var bcrypt = require('bcrypt-node');

var Ticket = function () {};

Ticket.new = function(order, user, callback) {
    console.log('--=[ TICKET INSERT:');

    Async.waterfall([
        function(callback) {
            var tickets = [];
            var called = false;
            for (var i = 0; i < order.ticket.amount; i++) {
                ticketHash(new Date(), function (err, hashcode) {
                    console.log(i, order.ticket.amount);
                    if (err) {
                        console.log('     FAILED! ]=--');
                        callback(err, null);
                    } else {
                        tickets.push({
                            type: 'test',
                            price: 2,
                            barcode: hashcode
                        });
                        if (i == order.ticket.amount && called == false) {
                            called = true;
                            callback(null, tickets);
                        }
                    }
                });
            }
        },
        function(tickets, callback) {
            var meals = [];
            if (order.lunch > 0) {
                for (var i = 0; i < order.lunch; i++) {
                    ticketHash(new Date(), function (err, hashcode) {
                        if (err) {
                            console.log('     FAILED! ]=--');
                            return callback(err, null);
                        }

                        meals.push({
                            type: 'lunch',
                            price: 2.5,
                            barcode: hashcode

                        });
                        if (i == order.lunch) {
                            callback(null, meals, tickets);
                        }
                    });

                }
            } else {
                callback(null, meals, tickets);
            }
        },
        function(meals, tickets, callback) {
            if (order.dinner > 0) {
                for (var i = 0; i < order.dinner; i++) {
                    ticketHash(new Date(), function (err, hashcode) {
                        if (err) {
                            console.log('     FAILED! ]=--');
                            return callback(err, null);
                        }

                        meals.push({
                            type: 'dinner',
                            price: 3.5,
                            barcode: hashcode

                        });
                        if (i == order.dinner) {
                            callback(null, meals, tickets);
                        }
                    });

                }
            } else {
                callback(null, meals, tickets);
            }
        },
        function(meals, tickets, callback) {
            var reservation_number = Math.floor(Math.random()*900000) + 100000;

            var Order = new TModel({
                reservation_number: reservation_number,
                firstname: user.firstname,
                infix: (user.infix !== null) ? user.infix : '',
                lastname: user.lastname,
                email: user.email,
                date: null,
                tickets: tickets,
                meals: meals
            });

            Order.save(function() {
                callback(null, reservation_number);
            });
        }
    ], function(err, reservation_number) {
        console.log('     SUCCESS! ]=--');
        return callback(null, reservation_number);
    });
};

Ticket.findBy = function(obj, callback) {
    TModel.find(obj, callback);
};

var ticketHash = function (param, callback) {
    var SALT_FACTOR = 5;
    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
        if (err) {
            return callback(err, null);
        }
        bcrypt.hash(param, salt, null, function (err, hash) {
            if (err) {
                return callback(err, null);
            }

            return callback(null, hash);
        })
    })
};

module.exports = Ticket;