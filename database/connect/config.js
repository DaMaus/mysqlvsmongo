module.exports = {
    "host": "127.0.0.1",
    "database": "mongotest",
    "user": "root",
    "password": "",
    "dbport": 3306,
    "port":  process.env.PORT || 3000
};