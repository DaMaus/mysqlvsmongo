var Async = require('async');
var mysqlDump = require('mysqldump');
var mysqlCon = require('./connection.js');

module.exports.getAll = function(table, callback) {
    var query = "SELECT * FROM " + table;
    this.executeSingleQuery(query, callback);
};

module.exports.insert = function(table, obj, callback) {
    var query = "INSERT INTO " + table + "(";
    var values = "";

    var i = 0;
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (i != 0) {
                query += ",";
                values += ",";
            }
            query += "`" + key + "`";
            values += "'" + obj[key] + "'";
        }
        i++;
    }

    query += ") VALUES(" + values + ")";

    this.testQuery(query, null, callback);
};

module.exports.backup = function(callback) {
    var currentdate = new Date();
    var date = currentdate.getDate() + ""
        + (currentdate.getMonth()+1)
        + currentdate.getFullYear()
        + currentdate.getHours()
        + currentdate.getMinutes()
        + currentdate.getSeconds();
    mysqlDump({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'mongotest',
        dest:'./backup/data' + date + '.sql' // destination file
    }, callback);
};

module.exports.executeSingleQuery = function(query, values, callback) {
    console.log('--=[ QUERY ');
    mysqlCon.connection(function (err, conn) {
        if (err) {
            console.log('     FAILED! ]=--');
            return callback(err);
        }
        conn.query(query, values, function (err, result) {
            if (err) {
                console.log('     FAILED! ]=--');
                return callback(err, null);
            }

            console.log('     SUCCESS! ]=--');

            return callback(null, result[0]);
        })
    })
};

module.exports.testQuery = function(query, values, callback) {
    console.log('--=[ QUERY ');
    var i = 500;
    mysqlCon.connection(function (err, conn) {
        if (err) {
            console.log('     FAILED! ]=--');
            return callback(err);
        } else {
            for (var k = 0; k < i; k++) {
                (function(k){
                    conn.query(query, values, function (err, result) {
                        if (err) {
                            console.log('     FAILED! ]=--');
                            return callback(err, null);
                        } else {
                            if (k == i -1) {
                                console(k);
                                return callback(null, null);
                            }
                        }
                    })
                }(k))
            }
        }

    });
};