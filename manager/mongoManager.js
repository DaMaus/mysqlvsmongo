var User = require('../database/mongodb/models/user');
var spawn = require('child_process').spawn;

var MoM = function(){};

MoM.testInsertPerformance = function(callback) {
    var i = 500;
    for (var k = 0; k < i; k++) {
        var Insert = new User({
            firstname: 'maurits',
            lastname: 'versluis',
            woonplaats: 'ameide'
        });
        (function(k){
            Insert.save(function(err, result) {
                if (k == i -1) {
                    callback(null, null);
                }
            });
        }(k))
    }
};

MoM.backup = function(callback) {
    var mongodump = spawn('C:\\Program Files\\MongoDB\\Server\\3.4\\bin\\mongodump', ['--db', 'test', '--collection', 'mongotests'])
    mongodump.stdout.on('data', function(data) {
        return callback(null, data);
    })
};

module.exports = MoM;