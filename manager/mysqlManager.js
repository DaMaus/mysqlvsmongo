var Mysql = require('../database/connect/mysql');
var MyM = function() {};

MyM.testInsertPerformance = function(callback) {
    Mysql.insert('user', {
        firstname: 'maurits',
        lastname: 'versluis',
        woonplaats: 'ameide'
    }, callback);
};

MyM.backup = function(callback) {
    Mysql.backup(callback);
};

module.exports = MyM;